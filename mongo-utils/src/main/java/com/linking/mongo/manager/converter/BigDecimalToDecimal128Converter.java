package com.linking.mongo.manager.converter;

import java.math.BigDecimal;
import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;

/**
 * @Author YaoWeiXin
 * @Date 2021/4/8 15:47
 * @Description BigDecimal转Decimal128
 */
@ReadingConverter
@WritingConverter
public class BigDecimalToDecimal128Converter implements Converter<BigDecimal, Decimal128> {

  @Override
  public Decimal128 convert(BigDecimal bigDecimal) {
    return new Decimal128(bigDecimal);
  }

}