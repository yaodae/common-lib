package com.linking.mongo.manager.converter;

import java.math.BigDecimal;
import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;

/**
 * @Author YaoWeiXin
 * @Date 2021/4/8 15:48
 * @Description Decimal128转BigDecimal
 */
@ReadingConverter
@WritingConverter
public class Decimal128ToBigDecimalConverter implements Converter<Decimal128, BigDecimal> {

  @Override
  public BigDecimal convert(Decimal128 decimal128) {
    return decimal128.bigDecimalValue();
  }


}
