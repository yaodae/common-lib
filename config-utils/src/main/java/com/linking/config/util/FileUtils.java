package com.linking.config.util;

/**
 * 文件工具类
 *
 * @author orange
 * @date 2019/03/27
 */
public class FileUtils extends org.apache.commons.io.FileUtils {

  public static String getFilePath(String path, String defaultPath) {
    String projectPath = System.getProperty("user.dir");
    projectPath += defaultPath;
    return projectPath + path;
  }

}
