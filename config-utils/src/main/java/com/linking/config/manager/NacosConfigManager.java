package com.linking.config.manager;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.linking.config.common.Constants;
import com.linking.config.pojo.BootstrapConfigBO;
import com.linking.config.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/15 11:27
 * @Description nacos配置管理器
 */
@Slf4j
public class NacosConfigManager {

  public static void loadConfig(String dataId, Consumer<String> saveFunc) throws NacosException {
    BootstrapConfigBO config = MainConfigManager.getBootstrapConfig();
    log.info("nacos server:{},{}", config.getNacosAddr(), config.getAppGroup());
    ConfigService configService = NacosFactory.createConfigService(config.getNacosAddr());
    String content = configService.getConfig(dataId, config.getAppGroup(), 5000);
    saveFunc.accept(content);
    log.info("{} is loadConfig:{}", dataId, content);
    configService.addListener(dataId, config.getAppGroup(), new Listener() {
      @Override
      public void receiveConfigInfo(String content) {
        saveFunc.accept(content);
        log.info("{} is receiveConfig:{}", dataId, content);
      }

      @Override
      public Executor getExecutor() {
        return null;
      }
    });
  }


}
