package com.linking.config.manager;

import com.alibaba.nacos.api.exception.NacosException;
import com.linking.config.pojo.BootstrapConfigBO;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author YaoWeiXin
 * @Date 2019/12/5 15:43
 * @Description 配置管理器
 */
@Slf4j
public class NormalConfigManager {

  public static void loadConfig(String name, Consumer<String> saveFunc) throws NacosException {
    BootstrapConfigBO configBo = MainConfigManager.getBootstrapConfig();
    NacosConfigManager.loadConfig(configBo.getAppDataId() + name, saveFunc);
  }

}
