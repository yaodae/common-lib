package com.linking.redis.aspect;

import com.linking.redis.manager.AbstractRedisManager;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/14 13:20
 * @Description redis切面
 */
@Aspect
@Component
@Slf4j
public class RedisAspect {

  @Pointcut("@annotation(com.linking.redis.annotation.RedisNameClear)")
  public void redisExecution() {
  }

  @After("redisExecution()")
  public void after() {
    log.info("RedisAspect is clear");
    AbstractRedisManager.removeCurName();
  }
}
