package com.borealis.common.utils.thread;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程工具类
 *
 * @author yaoweixin
 * @date 2019/1/28
 */
public class ThreadUtil {

  /**
   * 创建线程池
   */
  public static ThreadPoolExecutor createThreadPool(String threadName) {
    return new ThreadPoolExecutor(1, Integer.MAX_VALUE,
        60L, TimeUnit.SECONDS,
        new SynchronousQueue<>(), new CommonThreadFactory(threadName, false));
  }

  /**
   * 创建固定大小线程池
   */
  public static ThreadPoolExecutor createThreadPool(String threadName, int count) {
    return new ThreadPoolExecutor(1, count,
        60L, TimeUnit.SECONDS,
        new SynchronousQueue<>(), new CommonThreadFactory(threadName, false));
  }

  /**
   * 创建定时器线程池
   */
  public static ScheduledThreadPoolExecutor createNewScheduledExecutor(String threadName) {
    return new ScheduledThreadPoolExecutor(1, new CommonThreadFactory(threadName, false));
  }

}