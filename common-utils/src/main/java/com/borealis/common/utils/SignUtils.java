package com.borealis.common.utils;

import cn.hutool.core.convert.Convert;
import cn.hutool.crypto.SecureUtil;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author YaoWeiXin
 * @Date 2019/10/25 13:45
 * @Description 签名用工具类
 */
@Slf4j
public class SignUtils {

  /**
   * 将对象的参数进行签名
   *
   * @param t       对象
   * @param signKey 签名KEY
   * @return 签名字符串
   */
  public static <T> String sign(T t, String signKey) {
    return signs(t, signKey)[1];
  }

  /**
   * 将对象的参数进行签名
   *
   * @param t       对象
   * @param signKey 签名KEY
   * @return 签名字符串
   */
  public static <T> String[] signs(T t, String signKey) {
    Map<String, Object> map = DataUtils.object2Map(t);
    map = DataUtils.sortMapByKey(map);
    StringBuilder str = new StringBuilder();
    if (map != null) {
      for (String key : map.keySet()) {
        if ("sign".equals(key)) {
          continue;
        }
        Object value = map.get(key);
        if (value == null) {
          continue;
        }
        str.append(Convert.toStr(value));
      }
    }
    return new String[]{str.toString(), SecureUtil.md5(str.toString() + signKey)};
  }

  /**
   * 将MAP的参数进行签名
   *
   * @param map     参数集合
   * @param signKey 签名KEY
   * @return 签名字符串
   */
  public static String signKeyValue(Map<String, Object> map, String signKey) {
    map = DataUtils.sortMapByKey(map);
    StringBuilder str = new StringBuilder();
    if (map != null) {
      for (String key : map.keySet()) {
        String value = Convert.toStr(map.get(key));
        if (StringUtils.isEmpty(value)) {
          continue;
        }
        str.append(value);
      }
    }
    str.append(signKey);
    return SecureUtil.md5(str.toString());
  }

  /**
   * 将MAP的参数进行签名
   *
   * @param map     参数集合
   * @param signKey 签名KEY
   * @return 签名字符串
   */
  public static String signKeyValue2(Map<String, Object> map, String signKey) {
    map = DataUtils.sortMapByKey(map);
    StringBuilder str = new StringBuilder();
    if (map != null) {
      for (String key : map.keySet()) {
        if ("sign".equals(key)) {
          continue;
        }
        String value = Convert.toStr(map.get(key));
        if (StringUtils.isEmpty(value)) {
          continue;
        }
        str.append(key);
        str.append("=");
        str.append(value);
        str.append("&");
      }
    }
    str.append("key=");
    str.append(signKey);
    return SecureUtil.md5(str.toString()).toUpperCase();
  }
}
