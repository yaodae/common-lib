package com.borealis.common.utils;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @Author YaoWeiXin
 * @Date 2019/12/20 11:41
 * @Description 字符串工具类
 */
public class StringUtils extends StrUtil {

  public static boolean checkNotEmpty(String str) {
    return isNotBlank(str) && !"null".equalsIgnoreCase(str.trim()) && !"undefined"
        .equalsIgnoreCase(str.trim());
  }

  /**
   * 判断是否Base64编码
   *
   * @param str 字符串
   * @return true|false
   */
  public static boolean isBase64(String str) {
    String regex = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
    return ReUtil.isMatch(regex, str);
  }

  /**
   * 判断是否账号名
   *
   * @param str 字符串
   * @return true|false
   */
  public static boolean isAccountName(String str) {
    return !ReUtil.isMatch("/[^\\u4E00-\\u9FA5\\w]/", str);
  }

  /**
   * 判断是否邮箱名
   *
   * @param str 字符串
   * @return true|false
   */
  public static boolean isEmail(String str) {
    return Validator.isEmail(str);
  }
}
