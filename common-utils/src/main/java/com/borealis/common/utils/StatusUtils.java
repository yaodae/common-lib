package com.borealis.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 状态相关工具类
 *
 * @author yaoweixin
 * @date 2018/12/15
 */
public class StatusUtils {

  /**
   * 检测对应下标的状态
   *
   * @param str   状态字符串
   * @param index 下标
   * @return Boolean
   */
  public static Boolean test(String str, Integer index) {
    return StringUtils.isNotEmpty(str) && str.length() >= index &&
        str.charAt(index - 1) == '1';
  }

  /**
   * 修改状态
   *
   * @param str   状态字符串
   * @param index 下标
   * @param open  是否开启
   * @return String
   */
  public static String change(String str, Integer index, Boolean open) {
    if (StringUtils.isEmpty(str)) {
      str = "";
    }
    StringBuilder s = new StringBuilder(StringUtils.rightPad(str, index, "0"));
    return s.replace(index - 1, index, open ? "1" : "0").toString();
  }

  /**
   * 判断状态是否开启
   *
   * @return bool
   */
  public static boolean isEnable(int flag, int status) {
    return (flag & status) == status;
  }

  /**
   * 判断状态是否关闭
   *
   * @return bool
   */
  public static boolean isDisable(int flag, int status) {
    return (flag & status) == 0;
  }

  /**
   * 判断是否只有这个状态开启
   */
  public static boolean isOnlyEnable(int flag, int status) {
    return flag == status;
  }

  /**
   * 添加状态
   */
  public static int addStatus(int flag, int status) {
    return flag | status;
  }

  /**
   * 扣除状态
   */
  public static int deductStatus(int flag, int status) {
    return flag & ~status;
  }
}
