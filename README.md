# common-lib

#### 介绍
JAVA 底层封装库，mongo分装、redis分装、mysql封装、第三方支付封装

#### 软件架构
- common-utils=通用工具包，内存锁、签名工具、字符串工具等
- config-utils=配置工具包，封装了bootstrap.yml配置的读取与nacos配置的读取与监听
- controller-utils=控制器工具包，封装了通用过滤器，根据配置前缀可截取请求地址中的值作为app_id放入ThreadLocal
- linking-web-utils=网站工具包，封装了基础服务器的功能
- mongo-utils=mongo工具包，封装了mongo的多数据源与相关操作
- mysql-utils=mysql工具包，封装了mysql的多数据源与相关操作
- redis-utils=redis工具包，封装了redis的多数据源与相关操作
- third-party-utils=第三方支付工具包，封装了第三方支付的相关代码，支付宝、facebook、Google、IOS等

#### 安装教程

1.  使用私有仓库上传更新版本号即可

#### 使用说明

1.  使用gradle从私有仓库中下载

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
