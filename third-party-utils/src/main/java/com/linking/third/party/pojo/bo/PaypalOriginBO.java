package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/10/23 11:28
 * @Description paypal origin参数
 */
@Data
public class PaypalOriginBO {

  private String orderId;
  private String paypalOrderId;
  private String productId;
  private BigDecimal amount;
  private String paymentId;

  public static PaypalOriginBO of(String orderId, String productId, BigDecimal amount,
      String paymentId, String paypalOrderId) {
    PaypalOriginBO bo = new PaypalOriginBO();
    bo.setOrderId(orderId);
    bo.setProductId(productId);
    bo.setAmount(amount);
    bo.setPaymentId(paymentId);
    bo.setPaypalOrderId(paypalOrderId);
    return bo;
  }

}
