package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/10/21 11:37
 * @Description 米大师支付返回值参数
 */
@Data
public class MidasPayResBO {

  private Boolean ok;
  private String billNo;
  private Integer code;
  private String  msg;

  public static MidasPayResBO of(String billNo) {
    MidasPayResBO bo = new MidasPayResBO();
    bo.setOk(true);
    bo.setCode(0);
    bo.setBillNo(billNo);
    return bo;
  }

  public static MidasPayResBO ofFail(int code, String msg) {
    MidasPayResBO bo = new MidasPayResBO();
    bo.setOk(false);
    bo.setCode(code);
    bo.setMsg(msg);
    return bo;
  }
}
