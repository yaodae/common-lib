package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 微信支付参数
 */
@Data
public class WeChatPayBO {

  private String appId;
  private String appName;
  private String secret;
  private String mchId;
  private String productId;
  private String productDesc;
  private BigDecimal amount;
  private WeChatPayType payType;
  private String orderId;
  private String notifyUrl;
  private String clientIp;

  public static WeChatPayBO of(String appId, String appName, String secret, String mchId,
      String productId, String productDesc, BigDecimal amount, WeChatPayType payType,
      String orderId, String notifyUrl, String clientIp) {
    WeChatPayBO bo = new WeChatPayBO();
    bo.setAppId(appId);
    bo.setAppName(appName);
    bo.setSecret(secret);
    bo.setMchId(mchId);
    bo.setProductId(productId);
    bo.setProductDesc(productDesc);
    bo.setAmount(amount);
    bo.setPayType(payType);
    bo.setOrderId(orderId);
    bo.setNotifyUrl(notifyUrl);
    bo.setClientIp(clientIp);
    return bo;
  }

  /**
   * 支付宝支付类型类型
   */
  public enum WeChatPayType {

    /**
     * 应用支付
     */
    APP("APP"),

    ;
    public String name;

    WeChatPayType(String name) {
      this.name = name;
    }

  }
}
