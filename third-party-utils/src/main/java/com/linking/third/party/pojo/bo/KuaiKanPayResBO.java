package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/11/4 17:15
 * @Description 快看支付返回值参数
 */
@Data
public class KuaiKanPayResBO {

  private Boolean ok;
  private String billNo;
  private String transData;
  private String sign;
  private Integer code;
  private String  msg;

  public static KuaiKanPayResBO of(String billNo, String transData, String sign) {
    KuaiKanPayResBO bo = new KuaiKanPayResBO();
    bo.setOk(true);
    bo.setCode(0);
    bo.setBillNo(billNo);
    bo.setTransData(transData);
    bo.setSign(sign);
    return bo;
  }

  public static KuaiKanPayResBO ofFail(int code, String msg) {
    KuaiKanPayResBO bo = new KuaiKanPayResBO();
    bo.setOk(false);
    bo.setCode(code);
    bo.setMsg(msg);
    return bo;
  }
}
