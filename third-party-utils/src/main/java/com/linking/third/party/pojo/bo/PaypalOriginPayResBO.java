package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/10/26 15:51
 * @Description paypal原生支付返回值参数
 */
@Data
public class PaypalOriginPayResBO {

  private Boolean ok;
  private String links;
  private String tradeSeq;

  public static PaypalOriginPayResBO of(String links, String tradeSeq) {
    PaypalOriginPayResBO bo = new PaypalOriginPayResBO();
    bo.setOk(true);
    bo.setLinks(links);
    bo.setTradeSeq(tradeSeq);
    return bo;
  }

  public static PaypalOriginPayResBO ofFail() {
    PaypalOriginPayResBO bo = new PaypalOriginPayResBO();
    bo.setOk(false);
    return bo;
  }
}
