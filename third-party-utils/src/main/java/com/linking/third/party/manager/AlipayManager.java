package com.linking.third.party.manager;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.google.common.collect.Maps;
import com.linking.third.party.manager.alipay.AlipayBizContent;
import com.linking.third.party.manager.alipay.AlipayOrderInfoUtils;
import com.linking.third.party.pojo.bo.AlipayPayBO;
import com.linking.third.party.pojo.bo.AlipayPayBO.AlipayPayType;
import com.linking.third.party.pojo.bo.AlipayPayResBO;
import com.linking.third.party.pojo.bo.AlipayPayVerifyBO;
import com.linking.third.party.pojo.bo.ReceiptVerifyBO;
import java.math.BigDecimal;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 14:36
 * @Description 支付宝 管理类
 */
@Slf4j
public class AlipayManager {

  public static AlipayPayResBO doPay(AlipayPayBO alipayPay) {
    // SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
    if (alipayPay.getPayType() == AlipayPayType.APP) {
      AlipayBizContent content = new AlipayBizContent();
      content.setBody(alipayPay.getAppName() + "-" + alipayPay.getProductDesc());
      content.setSubject(alipayPay.getAppName() + "-" + alipayPay.getProductDesc());
      content.setOut_trade_no(alipayPay.getOrderId());
      content.setTotal_amount(alipayPay.getAmount().toPlainString());
      content.setTimeout_express("30m");
      content.setProduct_code("QUICK_MSECURITY_PAY");
      Map<String, String> params = AlipayOrderInfoUtils.buildOrderParamMap(alipayPay.getAppId(), alipayPay.getNotifyUrl(), content);
      String orderParam = AlipayOrderInfoUtils.buildOrderParam(params);
      String sign = AlipayOrderInfoUtils.getSign(params, alipayPay.getPrivateKey());
      String orderInfo = orderParam + "&" + sign;
      return AlipayPayResBO.of(orderInfo);
    }
    return AlipayPayResBO.ofFail();
  }

  /**
   * 票据验证
   *
   * @param alipayPayVerify 支付宝票据验证参数
   * @return 票据验证结果
   */
  public static ReceiptVerifyBO receiptVerify(AlipayPayVerifyBO alipayPayVerify) {
    try {
      // 1、组合参数
      Map<String, String> params = Maps.newHashMap();
      for (String name : alipayPayVerify.getParams().keySet()) {
        // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
        params.put(name, alipayPayVerify.getParams().getStr(name));
      }
      // 2、验证参数
      boolean flag = AlipaySignature.rsaCheckV1(params, alipayPayVerify.getPublicKey(), "UTF-8", "RSA2");
      if (!flag) {
        return ReceiptVerifyBO.ofFail();
      }
      String appId = alipayPayVerify.getParams().getStr("app_id");
      BigDecimal totalAmount = alipayPayVerify.getParams().getBigDecimal("total_amount");
      String sellerId = alipayPayVerify.getParams().getStr("seller_id");
      String tradeNo = alipayPayVerify.getParams().getStr("trade_no");
      String orderId = alipayPayVerify.getParams().getStr("out_trade_no");
      String tradeStatus = alipayPayVerify.getParams().getStr("trade_status");
      String tradeSuccess = "TRADE_SUCCESS";
      if (!tradeSuccess.equalsIgnoreCase(tradeStatus)) {
        return ReceiptVerifyBO.ofFail();
      }
      if (alipayPayVerify.getAlipayAppId().equals(appId) && alipayPayVerify.getAlipaySeller()
          .equals(sellerId)) {
        return ReceiptVerifyBO.ofWeChat(true, tradeNo, orderId);
      }
      return ReceiptVerifyBO.ofFail();
    } catch (AlipayApiException e) {
      log.error("receiptVerify error: ", e);
      return ReceiptVerifyBO.ofFail();
    }
  }
}
