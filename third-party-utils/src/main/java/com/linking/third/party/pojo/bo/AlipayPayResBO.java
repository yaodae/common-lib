package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 支付宝支付返回值参数
 */
@Data
public class AlipayPayResBO {

  private Boolean ok;
  private String body;

  public static AlipayPayResBO of(String body) {
    AlipayPayResBO bo = new AlipayPayResBO();
    bo.setOk(true);
    bo.setBody(body);
    return bo;
  }

  public static AlipayPayResBO ofFail() {
    AlipayPayResBO bo = new AlipayPayResBO();
    bo.setOk(false);
    return bo;
  }
}
