package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/11/4 17:36
 * @Description 快看支付校验参数
 */
@Data
public class KuaiKanPayVerifyBO {

  private String transData;
  private String sign;
  private Boolean sendBox;

  public static KuaiKanPayVerifyBO of(String transData, String sign, Boolean sendBox) {
    KuaiKanPayVerifyBO bo = new KuaiKanPayVerifyBO();
    bo.setTransData(transData);
    bo.setSign(sign);
    bo.setSendBox(sendBox);
    return bo;
  }

}
