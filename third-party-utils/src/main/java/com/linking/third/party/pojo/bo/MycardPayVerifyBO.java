package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/9/18 11:05
 * @Description mycard支付校验参数
 */
@Data
public class MycardPayVerifyBO {

  private String authCode;
  private String orderId;
  private BigDecimal amount;
  private Boolean sendBox;

  public static MycardPayVerifyBO of(String authCode, String orderId, BigDecimal amount,
      Boolean sendBox) {
    MycardPayVerifyBO bo = new MycardPayVerifyBO();
    bo.setAuthCode(authCode);
    bo.setOrderId(orderId);
    bo.setAmount(amount);
    bo.setSendBox(sendBox);
    return bo;
  }

}
