package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/11/4 17:09
 * @Description 快看支付参数
 */
@Data
public class KuaiKanPayBO {

  /**
   * 小程序 appId
   */
  private String appId;
  /**
   * 小程序 秘钥
   */
  private String appSecret;
  /**
   * 用户唯一标识符
   */
  private String openid;
  /**
   * 订单号，业务需要保证全局唯一；相同的订单号不会重复扣款。长度不超过63，只能是数字、大小写字母_-
   */
  private String orderId;
  /**
   * 快看支付结果通知服务器地址
   */
  private String notifyUrl;
  /**
   * 订单金额，不能为 0
   */
  private BigDecimal amount;
  /**
   * 道具名称
   */
  private String productDesc;
  /**
   * 是否沙盒模式
   */
  private Boolean sendBox;

  public static KuaiKanPayBO of(String appId, String appSecret, String openid, String orderId,
      BigDecimal amount, String productDesc, String notifyUrl, Boolean sendBox) {
    KuaiKanPayBO bo = new KuaiKanPayBO();
    bo.setAppId(appId);
    bo.setAppSecret(appSecret);
    bo.setOpenid(openid);
    bo.setOrderId(orderId);
    bo.setAmount(amount);
    bo.setProductDesc(productDesc);
    bo.setNotifyUrl(notifyUrl);
    bo.setSendBox(sendBox);
    return bo;
  }
}
