package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/9/16 17:51
 * @Description mycard支付返回值参数
 */
@Data
public class MycardPayResBO {

  private Boolean ok;
  private String authCode;
  private String tradeSeq;

  public static MycardPayResBO of(String authCode, String tradeSeq) {
    MycardPayResBO bo = new MycardPayResBO();
    bo.setOk(true);
    bo.setAuthCode(authCode);
    bo.setTradeSeq(tradeSeq);
    return bo;
  }

  public static MycardPayResBO ofFail() {
    MycardPayResBO bo = new MycardPayResBO();
    bo.setOk(false);
    return bo;
  }
}
