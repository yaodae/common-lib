package com.linking.third.party.pojo.bo;

import cn.hutool.json.JSONObject;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 微信支付校验参数
 */
@Data
public class WeChatPayVerifyBO {

  private JSONObject params;
  private String publicKey;

  public static WeChatPayVerifyBO of(JSONObject params, String publicKey) {
    WeChatPayVerifyBO bo = new WeChatPayVerifyBO();
    bo.setParams(params);
    bo.setPublicKey(publicKey);
    return bo;
  }

}
