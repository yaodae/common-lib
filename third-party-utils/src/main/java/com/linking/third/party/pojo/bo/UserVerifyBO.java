package com.linking.third.party.pojo.bo;

import com.alibaba.fastjson.JSONObject;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.types.User;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/2/18 16:41
 * @Description 票据验证参数
 */
@Data
public class UserVerifyBO {

  private Boolean verified;
  private String accessToken;
  private String refreshToken;
  private String userId;
  private String unionId;
  private String name;
  private String gender;
  private String picture;

  public static UserVerifyBO of(AccessToken accessToken, User user) {
    UserVerifyBO bo = new UserVerifyBO();
    bo.setVerified(true);
    bo.setAccessToken(accessToken.getAccessToken());
    bo.setUserId(user.getId());
    bo.setName(user.getName());
    bo.setGender(user.getGender());
    return bo;
  }

  public static UserVerifyBO of(Payload payload) {
    UserVerifyBO bo = new UserVerifyBO();
    bo.setVerified(true);
    bo.setAccessToken(payload.getAccessTokenHash());
    bo.setUserId(payload.getSubject());
    bo.setName((String) payload.get("name"));
    bo.setPicture((String) payload.get("picture"));
    return bo;
  }

  public static UserVerifyBO ofFireBase(String token, String userId) {
    UserVerifyBO bo = new UserVerifyBO();
    bo.setVerified(true);
    bo.setAccessToken(token);
    bo.setUserId(userId);
    return bo;
  }

  public static UserVerifyBO ofWeChat(String token, String refreshToken, String openid,
      String unionId, JSONObject userInfo) {
    UserVerifyBO bo = new UserVerifyBO();
    bo.setVerified(true);
    bo.setAccessToken(token);
    bo.setRefreshToken(refreshToken);
    bo.setUserId(openid);
    bo.setUnionId(unionId);
    if (userInfo != null) {
      bo.setName(userInfo.getString("nickname"));
      bo.setGender(userInfo.getString("sex"));
      bo.setPicture(userInfo.getString("headimgurl"));
    }
    return bo;
  }

  public static UserVerifyBO ofFail() {
    UserVerifyBO bo = new UserVerifyBO();
    bo.setVerified(false);
    return bo;
  }
}
