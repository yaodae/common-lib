package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/9/16 17:48
 * @Description mycard支付参数
 */
@Data
public class MycardPayBO {

  private String appId;
  private String appName;
  private String privateKey;
  private String publicKey;
  private String productId;
  private BigDecimal amount;
  private String productDesc;
  private MycardPayType payType;
  private String orderId;
  private String returnUrl;
  private String serverId;
  private String userId;
  private String currency;
  private Boolean sendBox;

  public static MycardPayBO of(String appId, String appName, String privateKey, String publicKey,
      String productId, BigDecimal amount, String productDesc, MycardPayType payType,
      String orderId, String returnUrl, String serverId, String userId, String currency,
      Boolean sendBox) {
    MycardPayBO bo = new MycardPayBO();
    bo.setAppId(appId);
    bo.setAppName(appName);
    bo.setPrivateKey(privateKey);
    bo.setPublicKey(publicKey);
    bo.setProductId(productId);
    bo.setAmount(amount);
    bo.setProductDesc(productDesc);
    bo.setPayType(payType);
    bo.setOrderId(orderId);
    bo.setReturnUrl(returnUrl);
    bo.setServerId(serverId);
    bo.setUserId(userId);
    bo.setCurrency(currency);
    bo.setSendBox(sendBox);
    return bo;
  }

  /**
   * Mycard支付类型类型
   */
  public enum MycardPayType {

    /**
     * 应用支付
     */
    APP("1"),
    /**
     * 网页支付
     */
    WEB("2"),

    ;
    public String name;

    MycardPayType(String name) {
      this.name = name;
    }

  }
}
