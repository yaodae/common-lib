package com.linking.third.party.manager.alipay;

import lombok.Data;

/**
 * 业务参数封装 
 * @author Javen
 * https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.mViJGu&treeId=193&articleId=105465&docType=1
 */
@Data
public class AlipayBizContent {

	/**
	 * 对一笔交易的具体描述信息
	 */
	private String body;
	/**
	 * 商品的标题
	 */
	private String subject;
	/**
	 * 商户网站唯一订单号
	 */
	private String out_trade_no;
	/**
	 * 订单总金额，单位为元，精确到小数点后两位
	 */
	private String total_amount;
	/**
	 * 固定制QUICK_MSECURITY_PAY
	 */
	private String product_code;
	/**
	 * 超时时间
	 */
	private String timeout_express;


}
