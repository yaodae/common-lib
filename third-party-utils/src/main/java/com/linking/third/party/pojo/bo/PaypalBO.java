package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/6/18 14:53
 * @Description paypal参数
 */
@Data
public class PaypalBO {

  private String orderId;
  private String productId;
  private BigDecimal amount;
  private String paymentMethodNonce;
  private String merchantAccountId;
  private Boolean log;

  public static PaypalBO of(String orderId, String productId, BigDecimal amount,
      String paymentMethodNonce, String merchantAccountId, Boolean log) {
    PaypalBO bo = new PaypalBO();
    bo.setOrderId(orderId);
    bo.setProductId(productId);
    bo.setAmount(amount);
    bo.setPaymentMethodNonce(paymentMethodNonce);
    bo.setMerchantAccountId(merchantAccountId);
    bo.setLog(log);
    return bo;
  }

}
