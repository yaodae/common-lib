package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/6/17 19:43
 * @Description google token参数
 */
@Data
public class GoogleTokenBO {

  private String accessToken;
  private Integer expiresIn;
  private Long createTime;

  public static GoogleTokenBO of(String accessToken, Integer expiresIn, Long createTime) {
    GoogleTokenBO bo = new GoogleTokenBO();
    bo.setAccessToken(accessToken);
    bo.setExpiresIn(expiresIn);
    bo.setCreateTime(createTime);
    return bo;
  }

}
