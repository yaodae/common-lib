package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/2/18 16:41
 * @Description 票据验证参数
 */
@Data
public class ReceiptVerifyBO {

  private Boolean verified;
  private String tradeNo;
  private String orderNo;
  private String productId;
  private Integer purchaseType;
  private Long expiryTime;
  private String paymentType;
  private String currency;
  private String myCardTradeNo;

  public static ReceiptVerifyBO of(Boolean verified, String tradeNo, String productId) {
    ReceiptVerifyBO bo = new ReceiptVerifyBO();
    bo.setVerified(verified);
    bo.setTradeNo(tradeNo);
    bo.setProductId(productId);
    bo.setExpiryTime(0L);
    return bo;
  }

  public static ReceiptVerifyBO of(Boolean verified, String tradeNo, String productId,
      Integer purchaseType) {
    ReceiptVerifyBO bo = of(verified, tradeNo, productId);
    bo.setPurchaseType(purchaseType);
    return bo;
  }

  public static ReceiptVerifyBO of(Boolean verified, String tradeNo, String productId,
      Integer purchaseType, Long expiryTime) {
    ReceiptVerifyBO bo = of(verified, tradeNo, productId);
    bo.setPurchaseType(purchaseType);
    bo.setExpiryTime(expiryTime);
    return bo;
  }

  public static ReceiptVerifyBO ofWeChat(Boolean verified, String tradeNo, String outTradeNo) {
    ReceiptVerifyBO bo = new ReceiptVerifyBO();
    bo.setVerified(verified);
    bo.setTradeNo(tradeNo);
    bo.setOrderNo(outTradeNo);
    bo.setExpiryTime(0L);
    return bo;
  }

  public static ReceiptVerifyBO ofMycard(Boolean verified, String paymentType, String currency,
      String myCardTradeNo) {
    ReceiptVerifyBO bo = new ReceiptVerifyBO();
    bo.setVerified(verified);
    bo.setPaymentType(paymentType);
    bo.setCurrency(currency);
    bo.setMyCardTradeNo(myCardTradeNo);
    bo.setExpiryTime(0L);
    return bo;
  }

  public static ReceiptVerifyBO ofKuaiKan(Boolean verified, String paymentType, String currency,
      String tradeNo) {
    ReceiptVerifyBO bo = new ReceiptVerifyBO();
    bo.setVerified(verified);
    bo.setPaymentType(paymentType);
    bo.setCurrency(currency);
    bo.setTradeNo(tradeNo);
    bo.setExpiryTime(0L);
    return bo;
  }

  public static ReceiptVerifyBO ofFail() {
    ReceiptVerifyBO bo = new ReceiptVerifyBO();
    bo.setVerified(false);
    return bo;
  }
}
