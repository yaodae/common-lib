package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/10/26 14:20
 * @Description paypal原生支付参数
 */
@Data
public class PaypalOriginPayBO {

  private String clientId;
  private String secret;
  private String productId;
  private BigDecimal amount;
  private String productDesc;
  private String orderId;
  private String returnUrl;
  private Boolean sendBox;

  public static PaypalOriginPayBO of(String clientId, String secret, String productId,
      BigDecimal amount, String productDesc, String orderId, String returnUrl, Boolean sendBox) {
    PaypalOriginPayBO bo = new PaypalOriginPayBO();
    bo.setClientId(clientId);
    bo.setSecret(secret);
    bo.setProductId(productId);
    bo.setAmount(amount);
    bo.setProductDesc(productDesc);
    bo.setOrderId(orderId);
    bo.setReturnUrl(returnUrl);
    bo.setSendBox(sendBox);
    return bo;
  }
}
