package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/10/21 10:36
 * @Description 米大师支付参数
 */
@Data
public class MidasPayBO {

  /**
   * 小程序 appId
   */
  private String appId;
  /**
   * 小程序 秘钥
   */
  private String appSecret;
  /**
   * 用户唯一标识符
   */
  private String openid;
  /**
   * 米大师分配的offer_id
   */
  private String offerId;
  /**
   * 游戏服务器大区id,游戏不分大区则默认zoneId ="1",String类型。如过应用选择支持角色，则角色ID接在分区ID号后用"_"连接。
   */
  private String zoneId;
  /**
   * 平台 安卓：android
   */
  private String pf;
  /**
   * 用户外网 IP
   */
  private String userIp;
  /**
   * 扣除游戏币数量，不能为 0
   */
  private BigDecimal amount;
  /**
   * 道具名称
   */
  private String productDesc;
  /**
   * 订单号，业务需要保证全局唯一；相同的订单号不会重复扣款。长度不超过63，只能是数字、大小写字母_-
   */
  private String orderId;
  /**
   * 备注。会写到账户流水
   */
  private String remark;
  /**
   * 接口调用凭证
   */
  private String accessToken;
  /**
   * 是否沙盒模式
   */
  private Boolean sendBox;
  /**
   * 是否需要日志
   */
  private Boolean isLog;

  public static MidasPayBO of(String appId, String appSecret, String openid, String offerId,
      String zoneId, String pf, String userIp, BigDecimal amount, String productDesc,
      String orderId, String remark, String accessToken, Boolean sendBox, Boolean isLog) {
    MidasPayBO bo = new MidasPayBO();
    bo.setAppId(appId);
    bo.setAppSecret(appSecret);
    bo.setOpenid(openid);
    bo.setOfferId(offerId);
    bo.setZoneId(zoneId);
    bo.setPf(pf);
    bo.setUserIp(userIp);
    bo.setAmount(amount);
    bo.setProductDesc(productDesc);
    bo.setOrderId(orderId);
    bo.setRemark(remark);
    bo.setAccessToken(accessToken);
    bo.setSendBox(sendBox);
    bo.setIsLog(isLog);
    return bo;
  }
}
