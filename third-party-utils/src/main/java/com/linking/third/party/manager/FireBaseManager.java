package com.linking.third.party.manager;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.linking.third.party.pojo.bo.UserVerifyBO;
import com.linking.third.party.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;

/** 1
 * @Author YaoWeiXin
 * @Date 2019/12/5 15:34
 * @Description FireBase 管理类
 */
@Slf4j
public class FireBaseManager {

  public static UserVerifyBO checkUserToken(String token) {
    DecodedJWT jwt = JwtUtils.checkToken(token);
    if (jwt == null) {
      return UserVerifyBO.ofFail();
    }
    return UserVerifyBO.ofFireBase(token, jwt.getSubject());
  }
}
