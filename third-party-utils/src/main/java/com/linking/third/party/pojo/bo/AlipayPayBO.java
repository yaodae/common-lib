package com.linking.third.party.pojo.bo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 支付宝支付参数
 */
@Data
public class AlipayPayBO {

  private String appId;
  private String appName;
  private String privateKey;
  private String publicKey;
  private String productId;
  private BigDecimal amount;
  private String productDesc;
  private AlipayPayType payType;
  private String orderId;
  private String notifyUrl;

  public static AlipayPayBO of(String appId, String appName, String privateKey, String publicKey, String productId,
      BigDecimal amount, String productDesc, AlipayPayType payType, String orderId, String notifyUrl) {
    AlipayPayBO bo = new AlipayPayBO();
    bo.setAppId(appId);
    bo.setAppName(appName);
    bo.setPrivateKey(privateKey);
    bo.setPublicKey(publicKey);
    bo.setProductId(productId);
    bo.setAmount(amount);
    bo.setProductDesc(productDesc);
    bo.setPayType(payType);
    bo.setOrderId(orderId);
    bo.setNotifyUrl(notifyUrl);
    return bo;
  }

  /**
   * 支付宝支付类型类型
   */
  public enum AlipayPayType {

    /**
     * 应用支付
     */
    APP("APP"),

    ;
    public String name;

    AlipayPayType(String name) {
      this.name = name;
    }

  }
}
