package com.linking.third.party.pojo.bo;

import java.util.Map;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 微信支付返回值参数
 */
@Data
public class WeChatPayResBO {

  private Boolean ok;
  private Map<String, Object> body;

  public static WeChatPayResBO of(Map<String, Object> data) {
    WeChatPayResBO bo = new WeChatPayResBO();
    bo.setOk(true);
    bo.setBody(data);
    return bo;
  }

  public static WeChatPayResBO ofFail() {
    WeChatPayResBO bo = new WeChatPayResBO();
    bo.setOk(false);
    return bo;
  }
}
