package com.linking.third.party.manager;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.linking.third.party.pojo.bo.UserVerifyBO;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.Version;
import com.restfb.types.User;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author YaoWeiXin
 * @Date 2019/12/5 15:34
 * @Description facebook 管理类
 */
@Slf4j
public class FaceBookManager {

  public static UserVerifyBO checkUserToken(String appId, String appSecret, String token) {
    // 1、检查token合法
    FacebookClient facebookClient = new DefaultFacebookClient(token, Version.LATEST);
    AccessToken accessToken = facebookClient.obtainExtendedAccessToken(appId, appSecret);
    // 2、检查token过期
    if (DateUtil.between(DateUtil.date(), accessToken.getExpires(), DateUnit.SECOND, false) <= 0) {
      return UserVerifyBO.ofFail();
    }
    // 3、获取用户
    User user = facebookClient.fetchObject("me", User.class);
    return UserVerifyBO.of(accessToken, user);
  }
}
