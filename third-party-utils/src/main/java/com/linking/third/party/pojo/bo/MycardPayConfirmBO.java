package com.linking.third.party.pojo.bo;

import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/9/18 11:03
 * @Description mycard支付确认参数
 */
@Data
public class MycardPayConfirmBO {

  private String authCode;
  private Boolean sendBox;

  public static MycardPayConfirmBO of(String authCode, Boolean sendBox) {
    MycardPayConfirmBO bo = new MycardPayConfirmBO();
    bo.setAuthCode(authCode);
    bo.setSendBox(sendBox);
    return bo;
  }

}
