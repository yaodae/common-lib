package com.linking.third.party.pojo.bo;

import cn.hutool.json.JSONObject;
import lombok.Data;

/**
 * @Author YaoWeiXin
 * @Date 2020/7/14 15:49
 * @Description 支付宝支付校验参数
 */
@Data
public class AlipayPayVerifyBO {

  private JSONObject params;
  private String publicKey;
  private String alipayAppId;
  private String alipaySeller;

  public static AlipayPayVerifyBO of(JSONObject params, String publicKey, String alipayAppId,
      String alipaySeller) {
    AlipayPayVerifyBO bo = new AlipayPayVerifyBO();
    bo.setParams(params);
    bo.setPublicKey(publicKey);
    bo.setAlipayAppId(alipayAppId);
    bo.setAlipaySeller(alipaySeller);
    return bo;
  }

}
