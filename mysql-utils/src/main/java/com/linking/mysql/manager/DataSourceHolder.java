package com.linking.mysql.manager;

/**
 * @Author YaoWeiXin
 * @Date 2020/8/14 9:51
 * @Description 动态数据源上下文
 */
public class DataSourceHolder {

  private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

  /**
   * 切换数据源
   */
  public static void setSource(String key) {
    CONTEXT_HOLDER.set(key);
  }

  /**
   * 获取数据源
   */
  public static String getSource() {
    return CONTEXT_HOLDER.get();
  }

  /**
   * 重置数据源
   */
  public static void clearSource() {
    CONTEXT_HOLDER.remove();
  }

}
