package com.linking.mysql.manager.mybatis;

import com.linking.mysql.manager.DataSourceHolder;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @Author YaoWeiXin
 * @Date 2020/8/14 9:58
 * @Description 动态数据源
 */
public abstract class AbstractMybatisDataSource extends AbstractRoutingDataSource {

  /**
   * 获取mysql管理器
   * @return  管理器
   */
  public abstract AbstractMybatisManager getMysqlManager();

  /**
   * 如果不希望数据源在启动配置时就加载好，可以定制这个方法，从任何你希望的地方读取并返回数据源
   * 比如从数据库、文件、外部接口等读取数据源信息，并最终返回一个DataSource实现类对象即可
   */
  @Override
  protected DataSource determineTargetDataSource() {
    return getMysqlManager().getTemplate(DataSourceHolder.getSource());
  }

  /**
   * 如果希望所有数据源在启动配置时就加载好，这里通过设置数据源Key值来切换数据，定制这个方法
   */
  @Override
  protected Object determineCurrentLookupKey() {
    return DataSourceHolder.getSource();
  }

  /**
   * 设置默认数据源
   */
  public void setDefaultDataSource(Object defaultDataSource) {
    super.setDefaultTargetDataSource(defaultDataSource);
  }

  /**
   * 设置数据源
   */
  public void setDataSources(Map<Object, Object> dataSources) {
    super.setTargetDataSources(dataSources);
  }

}