package com.linking.controller.common;


/**
 * @Author YaoWeiXin
 * @Date 2019/11/1 12:02
 * @Description 当前线程的应用编号
 */
public class AppIdHolder {

  private final static ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();

  public static void set(String po) {
    THREAD_LOCAL.set(po);
  }

  public static String get() {
    return THREAD_LOCAL.get();
  }

  public static void remove() {
    THREAD_LOCAL.remove();
  }

}