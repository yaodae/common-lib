package com.linking.controller.common;


import java.util.Locale;

/**
 * @Author YaoWeiXin
 * @Date 2019/11/1 12:02
 * @Description 当前线程的语言
 */
public class LocaleHolder {

  private final static ThreadLocal<Locale> THREAD_LOCAL = new ThreadLocal<>();

  public static void set(Locale locale) {
    THREAD_LOCAL.set(locale);
  }

  public static Locale get() {
    return THREAD_LOCAL.get();
  }

  public static void remove() {
    THREAD_LOCAL.remove();
  }

}