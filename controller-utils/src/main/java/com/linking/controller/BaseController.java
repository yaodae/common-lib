package com.linking.controller;

import com.alibaba.fastjson.JSONObject;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 * @Author YaoWeiXin
 * @Date 2020/4/14 17:50
 * @Description Controller基类
 */
@Slf4j
public abstract class BaseController {

  @Resource
  protected HttpServletRequest request;

  @Resource
  protected HttpServletResponse response;

  /**
   * 使用@ExceptionHandler注解，继承此类的Controller发生异常时会自动执行该方法
   * @param request   请求
   * @param e         错误
   * @return  返回值
   */
  @ExceptionHandler
  public abstract JSONObject exception(HttpServletRequest request, Exception e);

}