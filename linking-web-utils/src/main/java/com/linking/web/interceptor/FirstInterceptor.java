package com.linking.web.interceptor;

import com.linking.controller.common.AppIdHolder;
import com.linking.controller.common.LocaleHolder;
import com.linking.controller.interceptor.AbstractInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2019/10/31 17:40
 * @Description 接口拦截器
 */
@Component
@Slf4j
public class FirstInterceptor extends AbstractInterceptor {

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) {
    // 移除当前线程绑定的uid
    AppIdHolder.remove();
    LocaleHolder.remove();
  }
}
