package com.linking.web.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义异常
 *
 * @author licheng
 * @date 2018/7/5
 */
@Setter
@Getter
public class ServerStopException extends RuntimeException {

  private static final long serialVersionUID = 8207762781427574650L;

  public ServerStopException(String msg) {
    super(msg);
  }

  public ServerStopException(String msg, Exception e) {
    super(msg, e);
  }

  public ServerStopException(String msg, Throwable t) {
    super(msg, t);
  }

}
