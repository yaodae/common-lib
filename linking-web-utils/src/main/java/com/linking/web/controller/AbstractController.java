package com.linking.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.linking.controller.BaseController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/14 16:46
 * @Description 自身的控制器
 */
@RestController
@Slf4j
public abstract class AbstractController extends BaseController {

  protected HttpServletRequest getReq() {
    return request;
  }

  protected HttpServletResponse getRes() {
    return response;
  }

  @Override
  public JSONObject exception(HttpServletRequest request, Exception e) {
    log.error("MyController path:{}, exception:", request.getServletPath(), e);
    JSONObject object = new JSONObject();
    object.put("ret", -1);
    object.put("errorcode", "系统异常");
    return object;
  }
}
