package com.linking.web.pojo.vo;

import cn.hutool.core.date.DateUtil;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 对象基础类
 *
 * @author yaoweixin
 * @date 2019/02/12
 */
@Data
public class BaseVO implements Serializable {

  public String conventTimeToStr(Date date) {
    return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
  }

  public Long conventStrToTime(String dateStr) {
    try {
      if (StringUtils.isNotEmpty(dateStr)) {
        return DateUtil.parse(dateStr, "yyyy-MM-dd HH:mm:ss").getTime();
      }
    } catch (Exception e) {
      return 0L;
    }
    return 0L;
  }
}