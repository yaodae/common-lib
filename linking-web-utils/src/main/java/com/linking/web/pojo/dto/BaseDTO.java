package com.linking.web.pojo.dto;

import lombok.Data;


/**
 * 基础DTO
 *
 * @author yaoweixin
 * @date 2019/1/9
 */
@Data
public abstract class BaseDTO {

  private String ip;
}