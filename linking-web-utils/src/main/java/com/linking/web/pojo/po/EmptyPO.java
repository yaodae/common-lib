package com.linking.web.pojo.po;

/**
 * @Author YaoWeiXin
 * @Date 2019/11/6 15:34
 * @Description 空对象
 */
public class EmptyPO extends BasePO{

  @Override
  public Boolean testEmpty() {
    return true;
  }
}
