package com.linking.web.pojo.bo;

import java.io.Serializable;
import lombok.Data;

/**
 * 对象基础类
 *
 * @author yaoweixin
 * @date 2019/07/29
 */
@Data
public class BaseBO implements Serializable {

}