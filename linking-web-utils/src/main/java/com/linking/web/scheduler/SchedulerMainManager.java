package com.linking.web.scheduler;

import com.linking.web.common.AbstractBeans;
import com.linking.web.manager.redis.RedisSelfUtils;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2019/12/5 14:09
 * @Description 调度管理器
 */
@Component
@EnableScheduling
@EnableAsync
@Slf4j
public class SchedulerMainManager {

  @Resource
  RedisSelfUtils redisUtils;

  /**
   * 每30秒事件
   */
  @Async
  @Scheduled(cron = "*/30 * * * * ?")
  public void eventThirtySecond() {
    AbstractBeans.refreshIdWorker();
  }

}
