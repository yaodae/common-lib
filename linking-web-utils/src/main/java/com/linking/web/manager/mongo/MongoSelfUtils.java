package com.linking.web.manager.mongo;

import com.linking.mongo.manager.AbstractMongoUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/14 16:36
 * @Description mongo操作工具类
 */
@Component
@Slf4j
public class MongoSelfUtils extends AbstractMongoUtils {

}
