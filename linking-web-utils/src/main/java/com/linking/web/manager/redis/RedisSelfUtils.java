package com.linking.web.manager.redis;

import com.linking.redis.manager.AbstractRedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/14 10:59
 * @Description Redis自身管理类
 */
@Component
@Slf4j
public class RedisSelfUtils extends AbstractRedisUtils {

}
