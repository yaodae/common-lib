package com.linking.web.repository;

import com.borealis.common.utils.DataUtils;
import com.linking.web.manager.mongo.MongoSelfUtils;
import com.linking.web.pojo.bo.PageBO;
import com.linking.web.pojo.po.BasePO;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

/**
 * @Author YaoWeiXin
 * @Date 2019/11/27 11:50
 * @Description 分页数据的仓库基类
 */
@Repository
public abstract class AbstractPageListRepository<T extends BasePO> extends BaseRepository {

  @Resource
  MongoSelfUtils mongoUtils;

  /**
   * 获得数据库名
   *
   * @return 数据库名
   */
  protected abstract String getDbName();

  /**
   * 获得数据库集合名
   *
   * @return 数据库集合名
   */
  protected abstract String getCollection();

  /**
   * 获取对象class
   *
   * @return 类的class
   */
  protected abstract Class<T> getPoClass();

  /**
   * 获取主键KEY
   *
   * @return 主键KEY
   */
  protected abstract String primaryKey();

  /**
   * 获取集合查询KEY
   *
   * @return 集合查询KEY
   */
  protected abstract String listKey();

  /**
   * 根据玩家编号从数据库查询对象集合
   *
   * @param id 查询集合用编号
   * @return 结果
   */
  public List<T> queryList(String id) {
    return mongoUtils.findList(getPoClass(), DataUtils.parseMap(listKey(), id));
  }

  /**
   * 分页查询
   *
   * @param id     查询集合用编号
   * @param pageBo 分页信息
   * @return 集合
   */
  public List<T> queryPageList(String id, PageBO pageBo) {
    return mongoUtils.findList(getPoClass(), DataUtils.parseMap(listKey(), id), pageBo.getPage(),
        pageBo.getPageSize(), null);
  }

  /**
   * 根据主键编号从数据库查询对象
   *
   * @param id 主键编号
   * @return 结果
   */
  public T queryByPrimary(String id) {
    return mongoUtils.findByKey(getPoClass(), primaryKey(), id).orElse(null);
  }

  /**
   * DB层插入对象
   *
   * @param t 范型对象
   */
  public void insert(T t) {
    mongoUtils.insert(t);
  }


  /**
   * 查询总数
   *
   * @param id 查询集合用编号
   * @return 集合
   */
  public long queryCount(String id) {
    return mongoUtils.findCount(getPoClass(), DataUtils.parseMap(listKey(), id));
  }
}
