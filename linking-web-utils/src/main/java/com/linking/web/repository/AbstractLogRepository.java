package com.linking.web.repository;

import com.linking.web.pojo.po.BasePO;
import org.springframework.stereotype.Repository;

/**
 * 单个数据的仓库基类
 *
 * @author yaoweixin
 * @date 2019/2/13
 */
@Repository
public abstract class AbstractLogRepository<T extends BasePO> extends BaseRepository {

  /**
   * 获得数据库名
   *
   * @return 数据库名
   */
  protected abstract String getDbName();

  /**
   * 获得数据库集合名
   *
   * @return 数据库集合名
   */
  protected abstract String getCollection();

  /**
   * DB层插入对象
   *
   * @param t     范型对象
   */
  protected abstract void insert(T t);

  /**
   * 对象刷新至仓库
   * @param t   泛型对象
   */
  public void refresh(T t) {
    insert(t);
  }
}
