package com.linking.web.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

/**
 * IP管理工具类
 *
 * @author Licheng
 * @date 2017-12-26 9:19
 **/
public class IpUtils {

  /**
   * 未知
   */
  private static final String UNKNOWN = "unknown";

  /**
   * 本机IP地址
   */
  private static final String LOCALHOST = "127.0.0.1";

  /**
   * 本机Ipv6地址
   */
  private static final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";

  /**
   * 逗号
   */
  private static final String COMMA_SYMBOL = ",";

  /**
   * IP最大长度
   */
  private static final int MAX_IP_LENGTH = 15;

  public static String getIpAddress(HttpServletRequest request) {
    String ipAddress;
    try {
      ipAddress = request.getHeader("x-forwarded-for");
      if (ipAddress == null || ipAddress.length() == 0 || UNKNOWN.equalsIgnoreCase(ipAddress)) {
        ipAddress = request.getHeader("Proxy-Client-IP");
      }
      if (ipAddress == null || ipAddress.length() == 0 || UNKNOWN.equalsIgnoreCase(ipAddress)) {
        ipAddress = request.getHeader("WL-Proxy-Client-IP");
      }
      if (ipAddress == null || ipAddress.length() == 0 || UNKNOWN.equalsIgnoreCase(ipAddress)) {
        ipAddress = request.getRemoteAddr();
        if (LOCALHOST.equals(ipAddress) || LOCALHOST_IPV6.equals(ipAddress)) {
          // 根据网卡取本机配置的IP
          ipAddress = getLocalIp();
        }
      }
      // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
      // "***.***.***.***".length()
      if (ipAddress != null && ipAddress.length() > MAX_IP_LENGTH) {
        // = 15
        if (ipAddress.indexOf(COMMA_SYMBOL) > 0) {
          ipAddress = ipAddress.substring(0, ipAddress.indexOf(COMMA_SYMBOL));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      ipAddress = "";
    }
    return ipAddress;
  }

  /**
   * 判断是否是内网IP
   *
   * @param ip ip地址
   * @return 结果
   */
  public static boolean innerIp(String ip) {
    // 匹配10.0.0.0 - 10.255.255.255的网段
    // String pattern10 = "^(\\D)*10(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){3}";

    // 匹配172.16.0.0 - 172.31.255.255的网段
    // String pattern172 = "172\\.([1][6-9]|[2]\\d|3[01])(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){2}";

    // 匹配192.168.0.0 - 192.168.255.255的网段
    // String pattern192 = "192\\.168(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){2}";

    // 合起来写
    String pattern = "((192\\.168|172\\.([1][6-9]|[2]\\d|3[01]))"
        + "(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){2}|"
        + "^(\\D)*10(\\.([2][0-4]\\d|[2][5][0-5]|[01]?\\d?\\d)){3})";

    Pattern reg = Pattern.compile(pattern);
    Matcher match = reg.matcher(ip);

    return match.find();
  }

  /**
   * 获取本地机器IP地址
   *
   * @return 本地IP地址
   */
  public static String getLocalIp() {
    String localIp;
    InetAddress addr = null;
    try {
      addr = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    localIp = addr != null ? addr.getHostAddress() : "";
    return localIp;
  }

}
