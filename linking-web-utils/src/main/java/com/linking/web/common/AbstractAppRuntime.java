package com.linking.web.common;

import com.borealis.common.utils.setting.BaseRedisSettings;
import com.google.common.collect.Maps;
import com.linking.web.manager.redis.RedisSelfUtils;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

/**
 * 应用运行参数
 *
 * @author yaoweixin
 * @date 2019/1/21
 */
@Slf4j
public abstract class AbstractAppRuntime extends BaseRedisSettings {

  protected static AbstractAppRuntime appRuntime;

  public static AbstractAppRuntime getSelf() {
    return appRuntime;
  }

  @Resource
  RedisSelfUtils redisUtils;

  private static final String SET = "AppRuntime";
  private static final long SETTING_TIMEOUT = 30;
  private static final String APP_SETTING = "App:Setting:";

  @Override
  public String getSetName() {
    return SET;
  }

  @Override
  public Map<String, Object> getAllSettings(String key) {
    return redisUtils.entries(key);
  }

  @Override
  public void putSetting(String key, String field, Object value) {
    redisUtils.hmPutIfAbsent(key, field, value);
  }

  @Override
  public long getSettingTimeout() {
    return SETTING_TIMEOUT;
  }

  @Override
  public Map<String, Object> getDefault() {
    return Maps.newTreeMap();
  }

}
