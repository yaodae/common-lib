package com.linking.web.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/21 20:21
 * @Description 基础结果返回虚类
 */
@Data
public class BaseResult<T> {

  /**
   * @apiDefine success_msg 全局配置成功响应信息
   * @apiSuccess (200) {boolean} success 成功标识
   * @apiSuccess (200) {String} code 错误编号
   * @apiSuccess (200) {String} desc 错误说明
   * @apiSuccess (200) {Object} [data] 数据载体
   */
  protected boolean success = true;

  protected String code;

  protected String desc;

  protected T data;

  public boolean testFail() {
    return !success;
  }

  @Getter
  @AllArgsConstructor
  public enum CodeEnum {
    // 成功
    Success("0"),
    // 系统维护中
    Maintenance("9997"),
    // 接口异常
    Exception("9998"),
    // 系统繁忙
    Busy("9999"),

    ;

    private String code;
  }
}
