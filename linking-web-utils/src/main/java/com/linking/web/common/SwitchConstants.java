package com.linking.web.common;

/**
 * @Author YaoWeiXin
 * @Date 2021/4/12 13:41
 * @Description 开关常量
 */
public class SwitchConstants {

  private static Boolean IS_CACHE_OPEN = false;

  public static void cacheOpen(boolean flag) {
    IS_CACHE_OPEN = flag;
  }

  public static Boolean isCacheOpen() {
    return IS_CACHE_OPEN;
  }

}
