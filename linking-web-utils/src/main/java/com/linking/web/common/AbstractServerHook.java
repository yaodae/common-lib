package com.linking.web.common;

import com.linking.web.exception.ServerStopException;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author YaoWeiXin
 * @Date 2019/10/29 15:06
 * @Description 服务器关闭钩子
 */
@Slf4j
public abstract class AbstractServerHook implements Runnable {

  /** 停机维护全局变量，用户停止操作，以免数据不一致 */
  private static boolean isStop = false;

  /**
   * 清理资源
   */
  public abstract void clear();

  /**
   * 休眠时长
   * @return  毫秒
   */
  public abstract Long sleepTime();

  @Override
  public void run() {
    log.info("准备停机维护，进入等待时间");
    try {
      // 设置全局变量
      isStop = true;
      clear();
      // 等待5秒，准备维护
      Thread.sleep(sleepTime());
      log.info("等待时间结束，开始维护");
    } catch (InterruptedException e) {
      log.error("线程休眠出错：", e);
    }
    // 关闭服务器
    log.info("###################################################################");
    log.info("#####################  停机维护，服务已关闭   #####################");
    log.info("###################################################################");
  }

  /**
   * 停机维护，用户停止操作，以免数据不一致
   */
  public static void checkStop() {
    if (isStop) {
      throw new ServerStopException("即将停机维护，请退出游戏");
    }
  }

  /**
   * 停机维护，用户停止操作，以免数据不一致
   */
  public static Boolean isStop() {
    return isStop;
  }

}
