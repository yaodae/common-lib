package com.linking.web.common;

import com.linking.controller.common.LocaleHolder;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * @Author YaoWeiXin
 * @Date 2020/4/22 10:38
 * @Description 本地语言
 */
@Component
@Slf4j
public class LocaleManager {

  @Resource
  private MessageSource messageSource;

  /**
   * 根据指定的KEY活参数获取多语言字符串，通过LocaleHolder获得语言
   * @param msgKey    消息KEY
   * @param params    消息替换参数
   * @return  多语言字符串
   */
  public String getMessage(String msgKey, Object... params) {
    Object[] objs = new Object[params.length];
    System.arraycopy(params, 0, objs, 0, params.length);
    return messageSource.getMessage(msgKey, objs, LocaleHolder.get());
  }


}
