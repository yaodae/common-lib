package com.linking.web.configuration;

import com.linking.config.manager.MainConfigManager;
import com.linking.config.pojo.BootstrapConfigBO;
import com.linking.controller.filter.FirstFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author YaoWeiXin
 * @Date 2019/11/1 11:43
 * @Description 过滤器配置
 */
@Configuration
public class FilterConfiguration {

  @Bean
  public FilterRegistrationBean<FirstFilter> registerFirstFilter() {
    // 通过FilterRegistrationBean实例设置优先级可以生效
    // 通过@WebFilter无效
    BootstrapConfigBO mainConfig = MainConfigManager.getBootstrapConfig();
    FilterRegistrationBean<FirstFilter> bean = new FilterRegistrationBean<>();
    bean.setFilter(new FirstFilter());
    bean.setName("FirstFilter");
    bean.addUrlPatterns(mainConfig.getPrefixUrl() + "*");
    bean.setOrder(1);
    FirstFilter.prefixStr = mainConfig.getPrefixUrl();
    return bean;
  }

}
