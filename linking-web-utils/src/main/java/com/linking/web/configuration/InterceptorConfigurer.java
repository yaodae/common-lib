package com.linking.web.configuration;

import com.linking.web.interceptor.FirstInterceptor;
import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author YaoWeiXin
 * @Date 2019/10/31 18:06
 * @Description 拦截器配置
 */
@Configuration
public class InterceptorConfigurer implements WebMvcConfigurer {

  @Resource
  FirstInterceptor firstInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    InterceptorRegistration registration = registry.addInterceptor(firstInterceptor);
    registration.order(1);
    registration.addPathPatterns("/**");
    registration
        .excludePathPatterns("/", "/actuator", "/favicon.ico", "/css/**", "/script/**",
            "/image/**", "/fonts/**", "/img/**", "/locales/**", "/utils/**", "/vendor/**",
            "/**.js", "/error", "/index.html");
  }
}
